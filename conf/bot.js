module.exports = exports = {
    commandPrefix:    ':',
    commandsEnabled:  [ 'join', 'part' ],
    commandsAdmin:    [ 'kick', 'ban', 'unban', 'mute', 'unmute' ],
    // featuresEnabled:  [ 'voteMute', 'voteKick' ] (TODO: Add features.),
    admins:           [ 'Platzhalter' ],
    debug:            true
}