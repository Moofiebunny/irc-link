module.exports = exports = {
    nickname:  'TestBot92374',
    username:  'Testbot',
    realname:  'Official Testbot by Platzhalter',
    servers:   [
        /*{
            hostname: 'platzhalter.me',
            ssl:      true,
            port:     6697
        },*/
        {
            hostname: 'irc.scuttled.net',
            ssl:      true,
            port:     6697
        },
        {
            hostname: 'chat.freenode.net',
            ssl:      true,
            port:     6697
        }
    ]
}