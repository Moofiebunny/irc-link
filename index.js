var irc = require('irc');
var config  = require('./conf/irc.js');
var botconf = require('./conf/bot.js');
var servers = {  };

for(var i = 0; i < config.servers.length; i++) {
    var server = config.servers[i];
    if(server.hostname) {
        server.ssl = server.ssl ? true : false;
        server.port = server.port ? server.port : (server.ssl ? 6697 : 6667);
        
        servers[server.hostname] = [ new irc.Client(server.hostname, config.nickname, {
            port:        server.port,
            secure:      server.ssl,
            selfSigned:  true,
            certExpired: true,
            userName:    config.username,
            realName:    config.realname,
            channels:    [ '#bottest' ]
        }).on('registered', function() { console.log('Connected to ' + this.opt.server + '.') }) ];
    }
}

eachServer(function(o) {
  var server = o[0];
  server.addListener('raw', function(message) {
    if(message.args) for(var i = 0; i < message.args.length; i++) {
      if(message.args[i].indexOf('NETWORK=') > -1 && message.args[i].length > 8) o.push(message.args[i].split('NETWORK=')[1]);
    }
  });
});

function eachServer(cb) {
  for(var s in servers) {
    if(servers.hasOwnProperty(s)) cb(servers[s]);
  }
}

eachServer(function(o) {
  var server = o[0];
  server.addListener('pm', function(from, message) {
    if(message.indexOf(botconf.commandPrefix) == 0) {
      if(callbacks.hasOwnProperty('cmd')) {
        callbacks['cmd'](o[1], from, message.split(':')[1].split(' ')[0], message.split(':')[1].split(' ').slice(1, message.split(':')[1].split(' ').length));
      }
    }
    else {
      if(callbacks.hasOwnProperty('message')) {
        callbacks['message'](o[1], from, message);
      }
    }
  });
});

var callbacks = {};

var options = {
  on: function(evt, cb) {
    callbacks[evt] = cb;
  },
  getServerByHostname: function(hostname) {
    for(var s in servers) {
      if(servers[s][1] == hostname) return servers[s][0];
    }
    return null;
  },
  getServers: function() {
    return servers;
  }
}

require('./lib/listener')(options);








































/*var irc = require('irc');
var client = new irc.Client('platzhalter.me', 'TestBot', {
  channels: [ '#bottest' ],
  port: 6697,
  secure: true,
  selfSigned: true,
  userName: 'Testbot',
  realName: 'Official Testbot by Platzhalter',
  debug: false
});

client.addListener('raw', function(message) {
  if(message.args) for(var i = 0; i < message.args.length; i++) {
    if(message.args[i].indexOf('NETWORK=') > -1 && message.args[i].length > 8) console.log(message.args[i].split('NETWORK=')[1]);
  }
});

client.addListener('error', function(message) {
  console.log(message);
});*/