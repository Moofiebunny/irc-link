var botconf = require('../conf/bot.js');

var texts   = {
    'wrongcommand'  : 'There is no command named %c% this in our database. Maybe you mistyped it?',
    'joined'        : '%u% joined.',
    'parted'        : '%u% left the channel.',
    'toofewargs'    : 'Too few arguments.\nUsage: %u%',
    'toomanyargs'   : 'Too many arguments.\nUsage: %u%',
    'restricted'    : 'You do not have the access rights for this command. Please contact an administrator.'
}

var commands = {
 //  COMMAND : [  USAGE, NUMBER OF ARGS, DESCRIPTION, LIST TO USE                                       ]
    'help'   : [ 'help [command]'     ,1   ,'Displays the docs for given command'             ,     null],
    'join'   : [ 'join'               ,0   ,'Join the channel.'                               , {      }],
    'part'   : [ 'part'               ,0   ,'Leaves the channel.'                             ,     null],
    'kick'   : [ 'kick [username]'    ,1   ,'Kicks given user from the channel.'              ,     null],
    'ban'    : [ 'ban [username]'     ,1   ,'Bans given user from the channel.'               , {      }],
    'unban'  : [ 'unban [username]'   ,1   ,'Allows given user to join the channel again.'    ,     null],
    'mute'   : [ 'mute [username]'    ,1   ,'Forbids given user to talk.'                     , {      }],
    'unmute' : [ 'unmute [username]'  ,1   ,'Allows muted user to talk again.'                ,     null]
}

function d(message) {
    if(botconf.debug) console.log(message);
}

module.exports = exports = function(_) {
    function broadcast(message, except) {
        for(var u in commands.join[3]) {
            var serverName = u.split('@')[1];
            var user = u.split('@')[0];
            var server = _.getServerByHostname(serverName);
            if(server == 0) {
                return;
            }
            
            if(except != u) server.say(user, message);
        }
    }
    
    _.on('message', function(network, from, message) {
        if(commands.join[3].hasOwnProperty(from + '@' + network)) {
            broadcast('<' + from + '@' + network + '> ' + message, from + '@' + network);
        } else {
            var server = _.getServerByHostname(network);
            if(server != null) server.say(from, 'You are not joined to this channel!')
        }
        d('[MSG] ' + from + '@' + network + ': ' + message);
    });
    _.on('cmd', function(network, from, cmd, args) {
        var s = _.getServerByHostname(network);
        if(s == null) {
            d('[CMD] ' + from + '@' + network + ': ' + cmd + ' ' + args);
            return;
        }
        
        if(commands.hasOwnProperty(cmd)) {
            if(args.length == commands[cmd][1]) {
                switch (cmd) {
                    case 'help':
                        if(!commands.hasOwnProperty(args[0])) {
                            s.say(from, texts.wrongcommand.replace('%c%', args[0]));
                            d('[CMD] ' + from + '@' + network + ': ' + cmd + ' ' + args);
                            return;
                        }
                        s.say(from, 'Usage: ' + commands[args[0]][0]);
                        s.say(from, commands[args[0]][2]);
                        break;
                    case 'join':
                        if(commands.join[3].hasOwnProperty(from + '@' + network)) {
                            s.say(from, 'You already joined this channel.');
                            return;
                        }
                        commands.join[3][from + '@' + network] = true;
                        break;
                    case 'part':
                        if(commands.join[3].hasOwnProperty(from + '@' + network)) {
                            delete commands.join[3][from + '@' + network];
                        }
                        break;
                }
            } else
                s.say(from, (texts[(args.length > commands[cmd][1])] ? texts.toomanyargs : texts.toofewargs).replace('%u%', commands[cmd][0]))
        } else s.say(from, texts.wrongcommand.replace('%c%', cmd));
        d('[CMD] ' + from + '@' + network + ': ' + cmd + ' ' + args);
    })
};